@extends('layouts.admin')

@section('content')
<section class="content-header">
      <h1>
        CFP
        <small>Submissions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Submissions</li>
      </ol>
</section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>

            <div class="row">
              <form action="/create_topics" method="POST" id="CategoryCreate" enctype="multipart/form-data">
                    @csrf
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">Create Topics</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body pad">
                            <div class="form-group">Topic Name*</label>
                                <input type="text" name="topic_name" class="form-control" id="topic_name"
                                    @empty($editable) value="{{ old('topic_name') }}" @endempty @isset($editable)
                                    value="{{ $editable-topic_name }}" @endisset>
                                {!! $errors->first('topic_name', ' <label for="" class="text-danger">:message</label>')
                                !!}
                            </div>
                            <div class="form-group">
                                <label for="parent">Parent Topic</label>
                                <select name="parent_id" id="parent_id" class="form-control">
                                    <option value="0">--Main--</option>
                                    @foreach ($Topics as $x=>$Topics)
                                    <option value="{{ $Topics->id }}">{{ $Topics->topic_name }}</option>
                                    @endforeach
                                </select>
                                <script>
                                    var select=  document.getElementById("parent_id");
                                   select.value= @empty($editable) {{ old('parent_id')?old('parent_id'):0 }}  @endempty  @isset($editable) {{ $editable->parent_id }} @endisset;
                                    </script>

                            </div>

                            @empty($editable)
                            <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-eye"></i>
                                Publish</button>
                            @endempty
                            @isset($editable)
                            <button type="submit" class="btn btn-block btn-info "><i class="fa fa-pencil"></i>
                                Update</button>

                            @endisset


                        </div>
                    </div>
                </div>
                </form>

                <div class="col-xs-8">
                  <div class="box box-info">
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped data-table">
                        <thead>
                          <tr>
                            <th class="col-sm-1">#</th>
                            <th>Topic</th>
                            <th>Parent Topic</th>
                            <th class="col-sm-2">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($rows as $row)
                          @isset($row)
                          <tr>
                            <td>{{$loop->iteration }}</td>
                            <td>{{ $row->topic_name }}</td>
                            <td>@if($row->parent_id == 0) Main @else {{ $row->parent->topic_name}}@endif</td>
                            <td>
                              <button id = "delete" class="btn btn-danger btn-xs delete" data-id="{{ $row->id }}"><i class="fa fa-trash"
                                  aria-hidden="true"></i>
                              </button>
                            </td>
                            {{ $rows->links() }}
                          </tr>
                          @endisset
                          @endforeach

                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>

            </div>

</section>
<script>

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });

    $(document).ready(function () {
        $('.data-table').dataTable();
    });

    $(".delete").click(function (e) {
         e.preventDefault();
        var id = $(this).data("id");
        var con = confirm("Are You Sure!");
        if (con == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/create_topics/" + id,
                type: 'DELETE',
                data: {
                    "id": id,
                },
                success: function (response) {
                    window.location = '/create_topics';
                },
                error: function (xhr) {
                    alert('e')
                }
            });
        }
    });
    </script>
@endsection
