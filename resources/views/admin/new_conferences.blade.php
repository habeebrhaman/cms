@extends('layouts.admin')

@section('content')
<section class="content-header">
      <h1>
        New Conferences
        <small>requests</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">new conferences</li>
      </ol>
</section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>

            <div class="row">
                <div class="col-xs-12">
                  <div class="box box-info">
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped data-table">
                        <thead>
                          <tr>
                            <th class="col-sm-1">#</th>
                            <th>Name</th>
                            <th>Acronym</th>
                            <th>Venue</th>
                            <th>City</th>
                            <th>country</th>
                            <th>start date</th>
                            <th>end date</th>
                            <th>year</th>
                            <th>email</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($rows as $row)
                          @isset($row)
                          <tr>
                            <td>{{$loop->iteration }}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->acronym}}</td>
                            <td>{{$row->venue}}</td>
                            <td>{{$row->city}}</td>
                            <td>{{$row->country}}</td>
                            <td>{{$row->startdate}}</td>
                            <td>{{$row->enddate}}</td>
                            <td>{{$row->year}}</td>
                            <td>{{$row->email}}</td>
                            <td>
                              <button id = "delete" class="btn btn-danger btn-xs delete" data-id="{{ $row->id }}"><i class="fa fa-trash"
                                  aria-hidden="true"></i>
                              </button>
                              <a href="/accept_conference/{{ $row->id }}"><button id = "delete" class="btn btn-success btn-xs" data-id=""><i class="fa fa-check"
                                  aria-hidden="true"></i>
                              </button></a>
                            </td>

                          </tr>
                          @endisset
                          @endforeach

                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>

            </div>

</section>
<script>

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });

    $(document).ready(function () {
        $('.data-table').dataTable();
    });

    $(".delete").click(function (e) {
         e.preventDefault();
        var id = $(this).data("id");
        var con = confirm("Are You Sure!");
        if (con == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/create_topics/" + id,
                type: 'DELETE',
                data: {
                    "id": id,
                },
                success: function (response) {
                    window.location = '/create_topics';
                },
                error: function (xhr) {
                    alert('e')
                }
            });
        }
    });
    </script>
@endsection
