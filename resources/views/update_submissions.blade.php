@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h4><b>Hi, Welcome {{ Auth::user()->fname }}</b></h4></div>
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <form method="POST" action="/new_submission/{{$paper->id}}" aria-label="{{ __('CONFERENCE REGISTRATION') }}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf

                <div class="card-body">
                  <div class="card">
                      <div class="card-header">Title and Abstract</div>
                        <div class="card-body">

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title (*)') }}</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{$paper->title}}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="abstract" class="col-md-4 col-form-label text-md-right">{{ __('Abstract (*)') }}</label>
                            <div class="col-md-6">
                                <textarea rows="20" cols="50" name="abstract" id="abstract" required>
                                  {{$paper->abstract}}
                                </textarea>
                                @if ($errors->has('area'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                @endif
                         </div>
                       </div>
                      </div>
                        </div>
                        <hr class="m-y-md">
                        <div class="card">
                            <div class="card-header">KeyWords</div>
                              <div class="card-body">
                              <div class="form-group row">
                                  <label for="key" class="col-md-4 col-form-label text-md-right">{{ __('Keywords (*)') }}</label>
                                  <div class="col-md-6">
                                      <input id="key" type="text" class="form-control{{ $errors->has('key') ? ' is-invalid' : '' }}" name="key" value="{{$paper->keywords}}"  required autofocus>

                                      @if ($errors->has('key'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('key') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                                </div>
                              </div>
                              <hr class="m-y-md">
                              <div class="card">
                                  <div class="card-header">File Upload</div>
                                    <div class="card-body">

                                    <div class="form-group row">
                                        <label for="abstract_file" class="col-md-4 col-form-label text-md-right">{{ __('File (*)') }}</label>
                                        <div class="col-md-6">
                                            <input id="abstract_file" type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="abstract_file" value="{{ old('file') }}" required autofocus>
                                            <a href="{{asset('files/abstracts/'.$paper->file_name)}}">{{$paper->file_name}}</a>
                                            @if ($errors->has('file'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('file') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                      <div class="form-group row mb-0">
                                          <div class="col-md-6 offset-md-4">
                                              <button type="submit" class="btn btn-primary">
                                                  {{ __('Submit') }}
                                              </button>
                                          </div>
                                      </div>
                                      @if ($errors->any())
                                      <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                      </ul>
                                  @endif

                                  @if(session()->has('jsAlert'))
                                      <script>
                                          alert({{ session()->get('jsAlert') }});
                                      </script>
                                  @endif
                                    </div>

                </div>
            </div>
          </form>
        </div>
    </div>
</div>
@endsection
