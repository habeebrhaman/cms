@extends('layouts.app')

@section('content')
  <div class="nav-scroller py-3 mb-2 menu">
    <nav class="nav d-flex justify-content-between">
        <a class="p-2 text-muted" href="#"></a>
        <a class="p-2 text-muted" href="#"></a>
        <a class="p-2 text-muted" href="#"></a>
        <a class="p-2 text-muted" href="#">HOME</a>
        <a class="p-2 text-muted" href="#">ABOUT US</a>
        <a class="p-2 text-muted" href="#">NEWS</a>
        <a class="p-2 text-muted" href="{{URL::route('create-conference')}}">CREATE CONFERENCE</a>
        <a class="p-2 text-muted" href="#">CONTACT US</a>
        <a class="p-2 text-muted" href="#"></a>
        <a class="p-2 text-muted" href="#"></a>
        <a class="p-2 text-muted" href="#"></a>
      </nav>
      </div>
      <div class="container p-3 p-md-5 text-white rounded bg-color">
        <div class = "row">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic"></h1>
          <p class="lead my-3"></p>
          <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">MORE</a></p>
        </div>
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic"></h1>
          <p class="lead my-3"></p>
          <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">MORE</a></p>
        </div>
      </div>
      </div>
      <div class="brdr">
      </div>
      <div class="list container">
        <div class = "row">
          <div class="jumbotron col-md-4">
            <h3 class="display-3">About !!</h3>
            <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <hr class="m-y-md">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>

            </p>
          </div>
          <div class="col-md-8">
            <h3>ON Going Conferences</H3>
            <div class="list-group">
              @foreach($conference as $conf)
              @isset($conf)
                <a href="/conferences/{{$conf->id}}" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">{{$conf->name}}</h5>
                    <small>{{$conf->created_at->diffForHumans()}}</small>
                  </div>
                </a>
                @endisset
                @endforeach
              </div>
          </div>
        </div>
      <hr class="featurette-divider">
      </div>

      @if(session()->has('jsAlert'))
          <script>
              alert({{ session()->get('jsAlert') }});
          </script>
      @endif
      <footer class="container">
        <p>&copy; Editors ASSIST. &middot;</p>
      </footer>
@endsection
