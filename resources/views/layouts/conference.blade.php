<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Editorial Manager</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="{{asset('css/conference_css.css')}}">
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<body>

  <div class="col-sm-3">
   <div class="nav-side-menu">
     <div class="brand">Conference</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

        <div class="menu-list">

            <ul id="menu-content" class="menu-content collapse out">
                <li>
                  <a href="#">
                  <i class="fa fa-dashboard fa-lg"></i> Conference Home
                  </a>
                </li>
                <li>
                  <a href="/new_submission/{{$conference->id}}">
                  <i class="fa fa-dashboard fa-lg"></i> Registration
                  </a>
                </li>

            </ul>
     </div>
</div>

</div>
<div class="col-sm-9">
  <div class="well well-lg" style="text-align:center;">
  <h2>{{$conference->acronym}} / {{$conference->name}}</h2>
  </div>
  @yield('content')
</body>
</html>
