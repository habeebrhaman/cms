@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Conferences
        <small>Notifications</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">notifications</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

            <form action="/notifications" method="POST" id="chair_create" enctype="multipart/form-data">

                    @csrf
              <div class="row">
                  <div class="col-md-4">
                      <!-- category and image -->
                      <div class="box box-info">
                          <div class="box-header">

                              <h3 class="box-title">Conference Notifications</h3>

                          </div>

                          <div class="box-body pad">

                            <div class="form-group">
                                    <label for="title">Notification*</label>
                                    <input type="text" name="notification" class="form-control" onkeyup='saveValue(this);' id="notification">
                            </div>
                            <div class="form-group">
                                    <label for="title">links</label>
                                    <input type="text" name="links" class="form-control" onkeyup='saveValue(this);' id="links">
                            </div>
                            <div class="form-group">
                                 <label for="parent">for</label>
                                 <select name="for_id" id="for_id" class="form-control">
                                     <option value="0">--All--</option>
                                     <option value="1">Authors</option>
                                     <option value="2">Reviewers</option>

                                 </select>
                             </div>


                             <div class="col-4 pull-right">


                                 <button type="submit" class="btn btn-block btn-primary store"><i class="fa fa-eye"></i>
                                     Add</button>
                             </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-xs-8">
                    <div class="box box-info">
                      <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped data-table">
                          <thead>
                            <tr>
                              <th class="col-sm-1">#</th>
                              <th>Notification</th>
                              <th>For</th>
                              <th class="col-sm-2">Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach ($rows as $row)

                            @isset($row)
                            <tr>
                              <td>{{$loop->iteration }}</td>
                              <td>{{ $row->notification }}</td>
                              <td>@if($row->is_authors == 1)Authors @elseif($row->is_reviewers == 1) Reviewers @else All @endif</td>
                              <td>
                                <button id = "delete" class="btn btn-danger btn-xs delete" data-id="{{ $row->id }}"><i class="fa fa-trash"
                                    aria-hidden="true"></i>
                                </button>
                              </td>
                            </tr>
                            @endisset
                            @endforeach
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>

                </div>
       </form>

        <script>
        $(document).ready(function(){
          $(".delete").click(function (e) {

        var id = $(this).data("id");
        var con = confirm("Are You Sure!");
        if (con == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/notifications/" + id,
                type: 'DELETE',
                data: {
                    "id": id,
                },
                success: function (response) {
                     location.reload();
                },
                error: function (xhr) {
                    alert('e')
                }
            });
            e.preventDefault();
        }
    });
        });
      </script>

</section>
@endsection
