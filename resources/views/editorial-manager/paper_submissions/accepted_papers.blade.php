@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        CFP
        <small>Submissions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Submissions</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
      @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
      @endif
     </div>
        <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Responsive Hover Table</h3>

                    <div class="box-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                      <tr>
                        <th>#</th>
                        <th>Paper Id</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                      @foreach($papers as $paper)
                      @isset($paper)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$paper->paper_generated_id}}</td>
                        <td>{{$paper->paper->title}}</td>
                        <td>@if($paper->paper->review_status == 0)
                            <span class="label label-success">Strong Accept</span>
                            @elseif($paper->paper->review_status == 1)
                            <span class="label label-primary">Accept</span>
                            @elseif($paper->paper->review_status == 2)
                            <span class="label label-warning">Reject</span>
                            @else
                            <span class="label label-danger">Strong Reject</span>
                            @endif
                        </td>
                        <td><a href="/submission_details/{{$paper->paper->id}}" ><span class="label label-success">View</span></a>
                      @isset($paper->mailSender->accept_mail_send)  @if($paper->mailSender->accept_mail_send == "1") @endisset<span class="label label-primary">Mail Sended</span> @else<a href="/send_accept_mail/{{$paper->id}}" ><span class="label label-primary">Send Accept Mail</span></a> @endif</td>
                      </tr>
                      @endisset
                      @endforeach
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
          </div>


  </section>
  <script>
  $("document").ready(function(){
    setTimeout(function(){
        $(".flash-message").remove();
    }, 5000 ); // 5 secs

});
  </script>

@endsection
