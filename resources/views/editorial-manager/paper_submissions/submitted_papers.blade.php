@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        CFP
        <small>Submissions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Submissions</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
        <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Responsive Hover Table</h3>

                    <div class="box-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                      <tr>
                        <th>#</th>
                        <th>Author</th>
                        <th>Organization</th>
                        <th>Email</th>
                        <th>Title</th>
                        <th>Action</th>
                      </tr>
                      @foreach($papers as $paper)
                      @isset($paper)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$paper->author->fname}}</td>
                        <td>{{$paper->author->organization}}</td>
                        <td>{{$paper->author->email}}</td>
                        <td>{{$paper->title}}</td>
                        <td><a href="/submission_details/{{$paper->id}}" ><span class="label label-danger">View</span></a>
                            </td>
                      </tr>
                      @endisset
                      @endforeach
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
          </div>


  </section>


@endsection
