@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Final Papers
        <small>Submissions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Final Submissions</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
      @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
      @endif
     </div>
        <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Papers</h3>

                    <div class="box-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                      <tr>
                        <th>#</th>
                        <th>Paper Id</th>
                        <th>File</th>
                        <th>Action</th>
                      </tr>
                      @foreach($papers as $paper)
                      @isset($paper)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$paper->final_id}}</td>
                        <td><a href="{{asset('files/final/'.$paper->filename)}}" class="form-control" >{{$paper->filename}}</a></td>

                        <td><a href="#" ><span class="label label-success">View</span></a>
                        </td>
                      </tr>
                      @endisset
                      @endforeach
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
          </div>


  </section>
  <script>
  $("document").ready(function(){
    setTimeout(function(){
        $(".flash-message").remove();
    }, 5000 ); // 5 secs

});
  </script>

@endsection
