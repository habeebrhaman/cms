@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Conferences
        <small>Conference Chair</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Chair</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

            <form action="/chair" method="POST" id="chair_create" enctype="multipart/form-data">
               
                    @csrf
              <div class="row">
                  <div class="col-md-4">
                      <!-- category and image -->
                      <div class="box box-info">
                          <div class="box-header">
                              
                              <h3 class="box-title">Conference Chair</h3>
                              
                          </div>
                          
                          <div class="box-body pad">
                            
                            <div class="form-group">
                                    <label for="title">Name*</label>
                                    <input type="text" name="name" class="form-control" onkeyup='saveValue(this);' id="projects_name">
                            </div>
                            <div class="form-group">
                                 <label for="parent">Session</label>
                                 <select name="session_id" id="session_id" class="form-control">
                                     <option value="0">--Session--</option>
                                     <option value="1">9.30 - 10-30</option>
                                     
                                 </select>
                             </div>
                               
                             
                             <div class="col-4 pull-right">


                                 <button type="submit" class="btn btn-block btn-primary store"><i class="fa fa-eye"></i>
                                     Add</button>
                             </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-xs-8">
                    <div class="box box-info">
                      <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped data-table">
                          <thead>
                            <tr>
                              <th class="col-sm-1">#</th>
                              <th>Name</th>
                              <th>Session</th>
                              <th class="col-sm-2">Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach ($rows as $row)

                            @isset($row)
                            <tr>
                              <td>{{$loop->iteration }}</td>
                              <td>{{ $row->name }}</td>
                              <td>{{ $row->program_sessions_id }}</td>
                              <td>
                                <button id = "delete" class="btn btn-danger btn-xs delete" data-id="{{ $row->id }}"><i class="fa fa-trash"
                                    aria-hidden="true"></i>
                                </button>
                              </td>
                            </tr>
                            @endisset
                            @endforeach
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>

                </div>
       </form>
       
        <script>
        $(document).ready(function(){
          $(".delete").click(function (e) {
            
        var id = $(this).data("id");
        var con = confirm("Are You Sure!");
        if (con == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/chair/" + id,
                type: 'DELETE',
                data: {
                    "id": id,
                },
                success: function (response) {
                     location.reload();
                },
                error: function (xhr) {
                    alert('e')
                }
            });
            e.preventDefault();
        }
    });
        });
      </script>
      
</section>
@endsection
