@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Reviewers
        <small>all</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reviewers</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
        <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Manage Reviewer</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">

                  </div>
                  <!-- /.box-body -->
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-xs-8">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Topics</h3>
                    </div>
                    <div class="box-body pad">
                      <table class="table table-hover">
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        @foreach($rows as $row)
                        @isset($row)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{$row->reviewer->fname}}</td>
                          <td>{{$row->reviewer->fname}}</td>
                          <td> status</td>
                          <td> action</td>
                        </tr>
                        @endisset
                        @endforeach
                      </table>
                    </div>
                </div>
              <!-- /.box -->
            </div>
            <div class="col-md-4">
                <!-- category and image -->

            </div>

          </div>

  </section>


@endsection
