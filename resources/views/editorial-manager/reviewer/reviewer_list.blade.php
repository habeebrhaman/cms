@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Reviewers
        <small>all</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reviewers</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
        <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Reviewers List</h3>

                    <div class="box-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Organization</th>
                        <th>Affliation</th>
                        <th>Country</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                      @foreach($rows as $row)
                      @isset($row)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$row->fname}}</td>
                        <td>{{$row->organization}}</td>
                        <td>{{$row->affliation}}</td>
                        <td>{{$row->country}}</td>
                        <td>{{$row->phone}}</td>
                        <td>{{$row->email}}</td>
                        <td>
                          <a href="/reviewers_list/{{$row->id}}" ><span class="label label-success">View</span></a>
                        </td>
                      </tr>
                      @endisset
                      @endforeach
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
          </div>


  </section>


@endsection
