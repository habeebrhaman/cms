@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Review
        <small>Papers</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Review Papers</li>
      </ol>
</section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
            <div class="row">
                <div class="col-xs-8">
                  <div class="box box-info">
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped data-table">
                        <thead>
                          <tr>
                            <th class="col-sm-1">#</th>
                            <th>Paper Title</th>
                            <th>Reviewer</th>
                            <th>Status</th>
                            <th>Revision Status</th>
                            <th class="col-sm-2">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($rows as $row)

                          @isset($row)
                          <tr>
                            <td>{{$loop->iteration }}</td>
                            <td>{{$row->paper->title}}</td>
                            <td>{{$row->reviewer->fname}}</td>
                            <td>@if($row->reviews->status == 0)
                                <span class="label label-success">Strong Accept</span>
                                @elseif($row->reviews->status == 1)
                                <span class="label label-primary">Accept</span>
                                @elseif($row->reviews->status == 2)
                                <span class="label label-warning">Reject</span>
                                @else
                                <span class="label label-danger">Strong Reject</span>
                                @endif
                            </td>
                            <td>@if($row->rev_status > 0) Revision-{{$row->rev_status}} @else No Revision @endif</td>
                            <td><a href="/reviews_details/{{$row->id}}" ><span class="label label-success">View</span></a>

                            </td>
                          </tr>
                          @endisset
                          @endforeach

                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>

            </div>
</section>
@endsection
