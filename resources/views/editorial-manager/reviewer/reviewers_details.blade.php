@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Reviewers
        <small>all</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reviewers</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
        <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Reviewer Details</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">
                     <table class="table table-hover">
                       <tr>
                       <td>Name :</td>
                       <td>{{$row->fname}} &nbsp{{$row->lname}}</td>
                     </tr>

                       <tr>
                       <td>Organization :</td>
                       <td>{{$row->organization}}</td>
                     </tr>

                       <tr>
                       <td>Affliation :</td>
                       <td>{{$row->affliation}}</td>
                     </tr>

                       <tr>
                       <td>Countdy :</td>
                       <td>{{$row->country}}</td>
                     </tr>

                       <tr>
                       <td>Web :</td>
                       <td>{{$row->web}}</td>
                     </tr>

                       <tr>
                       <td>Phone :</td>
                       <td>{{$row->phone}}</td>
                     </tr>

                       <tr>
                       <td>Email :</td>
                       <td>{{$row->email}}</td>
                     </tr>
                     </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <a href="/request_to_reviewer/{{$row->id}}" ><span class="label label-success">Send Request for review</span></a>
                <!-- /.box -->
              </div>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="box box-info">
                <div class="box-body">

                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
          </div>
            <div class="col-md-4">
                <!-- category and image -->
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Topics</h3>
                    </div>
                    <div class="box-body pad">
                      <table class="table table-hover">
                        <tr>
                          <th>#</th>
                          <th>Topics</th>
                        </tr>
                        @foreach($topic as $top)
                        @isset($top)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{$top->topics->topic_name}}</td>
                        </tr>
                        @endisset
                        @endforeach
                      </table>
                    </div>
                </div>
            </div>

          </div>

  </section>


@endsection
