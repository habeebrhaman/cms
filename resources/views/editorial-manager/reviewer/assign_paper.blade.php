@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Reviewers
        <small>all</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reviewers</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
        <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Manage Reviewer</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body table-responsive no-padding">

                  </div>
                  <!-- /.box-body -->
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <form action="/assign_to_reviewer" method="POST" id="CategoryCreate" enctype="multipart/form-data">
                         @csrf
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Assign Paper to Reviewer</h3>
                    </div>
                    <div class="box-body pad">
                      <div class="form-group">
                          <label for="paper_id">Paper Name</label>
                          <select name="paper_id" id="paper_id" class="form-control">
                              @foreach ($papers as $x=>$paper)
                                @isset($paper)
                                 <option value="{{ $paper->id }}" @isset($old){{$old->paper_id == $paper->id ? "selected":"" }} @endisset>{{ $paper->title }}</option>
                                @endisset
                              @endforeach
                          </select>

                      </div>

                      <div class="form-group">
                          <label for="reviewer_id">Reviewer Name</label>
                          <select name="reviewer_id" id="reviewer_id" class="form-control">
                              @foreach ($reviewer as $x=>$review)
                                @isset($review)
                                 <option value="{{ $review->id }}" @isset($old){{$old->reviewer_id == $review->id ? "selected":"" }} @endisset>{{ $review->fname }}</option>
                                @endisset
                              @endforeach
                          </select>

                      </div>
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-eye"></i>
                            Assign</button>
                     </div>
                    </div>
                  </form>
                </div>
            <div class="col-md-6">
              <div class="box box-info">
                  <div class="box-header">
                      <h3 class="box-title">Asignment Details</h3>
                  </div>
                  <div class="box-body pad">
                    <table class="table table-hover">
                      <tr>
                        <th>#</th>
                        <th>Paper Name</th>
                        <th>Reviewer Name</th>
                        <th>Action</th>
                      </tr>
                      @foreach($rows as $row)
                      @isset($row)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$row->paper->title}}</td>
                        <td>{{$row->reviewer->fname}}</td>
                        <td>
                          <button id = "delete" class="btn btn-danger btn-xs delete" data-id="{{ $row->id }}"><i class="fa fa-trash"
                              aria-hidden="true"></i>
                          </button>
                        </td>
                      </tr>
                      @endisset
                      @endforeach
                    </table>
                  </div>
              </div>

            </div>

          </div>

  </section>
  <script>

      $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });

      $(document).ready(function () {
          $('.data-table').dataTable();
      });

      $(".delete").click(function (e) {
           e.preventDefault();
          var id = $(this).data("id");
          var con = confirm("Are You Sure!");
          if (con == true) {
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
              $.ajax({
                  url: "/reviewers_list/" + id,
                  type: 'DELETE',
                  data: {
                      "id": id,
                  },
                  success: function (response) {
                      window.location = '/paper_assign';
                  },
                  error: function (xhr) {
                      alert('e')
                  }
              });
          }
      });
      </script>


@endsection
