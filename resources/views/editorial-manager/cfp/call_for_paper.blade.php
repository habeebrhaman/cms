@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Conferences
        <small>Call For Paper</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Call For Paper</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
       @if($update == 0)
          <form action="/call_for_paper" method="POST" id="chair_create" enctype="multipart/form-data">
       @else
         @isset($id)
          <form action="/call_for_paper/{{$id->id}}" method="POST" id="chair_create" enctype="multipart/form-data">
          @method('PUT')
         @endisset
       @endif

                    @csrf
              <div class="row">
                  <div class="col-md-4">
                      <!-- category and image -->
                      <div class="box box-info">
                          <div class="box-header">

                              <h3 class="box-title">Add Call For Paper</h3>

                          </div>

                          <div class="box-body pad">

                            <div class="form-group">
                                    <label for="s_date">Submission Date</label>
                                    <input type="date" name="s_date" class="form-control" onkeyup='saveValue(this);' id="s_date">
                            </div>
                            <div class="form-group">
                                    <label for="e_date">End Date</label>
                                    <input type="date" name="e_date" class="form-control" onkeyup='saveValue(this);' id="e_date">
                            </div>
                            <div class="form-group">
                                    <label for="ex_date">Extended Date</label>
                                    <input type="date" name="ex_date" class="form-control" onkeyup='saveValue(this);' id="ex_date">
                            </div>
                            <div class="form-group">
                                    <label for="p_area">Primary Area</label>
                                    <select name="primary_id" id="primary_id" class="form-control">
                                      @foreach ($Topics as $x=>$Topic)
                                      <option value="{{ $Topic->id }}">{{ $Topic->topic_name }}</option>
                                      @endforeach
                                 </select>
                            </div>
                            <div class="form-group">
                                    <label for="s_area">Secondary Area</label>
                                    <select name="secondary_id" id="secondary_id" class="form-control">
                                      @foreach ($Topics as $x=>$Topic)
                                      <option value="{{ $Topic->id }}">{{ $Topic->topic_name }}</option>
                                      @endforeach
                                 </select>
                            </div>
                            <div class="form-group">
                                    <label for="notes">Area Notes</label>
                                    <input type="text" name="notes" class="form-control" onkeyup='saveValue(this);' id="notes">
                            </div>

                             <div class="col-4 pull-right">

                                 <button type="submit" class="btn btn-block btn-primary store"><i class="fa fa-eye"></i>
                            @if($update==0)Add @else Update @endif</button>
                             </div>
                          </div>
                      </div>
                  </div>
                  @isset($row)
                  <div class="col-xs-8">
                    <div class="box box-info">
                      <div class="box-body">
                      <div class="form-group">

                        <label for="e_date">Conference</label>
                        <input type="text" readonly class="form-control" value="{{$row->conferences_id}}">
                      </div>
                      <div class="form-group">
                        <label for="e_date">Submission Date</label>
                        <input type="text" readonly class="form-control" value="{{$row->submission_date}}">
                      </div>
                      <div class="form-group">
                        <label for="e_date">End Date</label>
                        <input type="text" readonly class="form-control" value="{{$row->end_date}}">
                      </div>
                      <div class="form-group">
                        <label for="e_date">Primary Area</label>
                        <input type="text" readonly class="form-control" value="@isset($row->topic1->topic_name){{$row->topic1->topic_name}}@endisset">
                      </div>
                      <div class="form-group">
                        <label for="e_date">Secondary Area</label>
                        <input type="text" readonly class="form-control" value="@isset($row->topic1->topic_name){{$row->topic2->topic_name}}@endisset">
                      </div>
                      <div class="form-group">
                        <label for="e_date">Note</label>
                        <input type="text" readonly class="form-control" value="{{$row->area_note}}">
                      </div>
                      <div class="form-group">
                        <label for="e_date">Extended Date</label>
                        <input type="text" readonly class="form-control" value="{{$row->extended_date}}">
                      </div>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>

                 @endisset
                    <!-- /.box -->
                  </div>

                </div>
       </form>

        <script>
        $(document).ready(function(){
          $(".delete").click(function (e) {

        var id = $(this).data("id");
        var con = confirm("Are You Sure!");
        if (con == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/chair/" + id,
                type: 'DELETE',
                data: {
                    "id": id,
                },
                success: function (response) {
                     location.reload();
                },
                error: function (xhr) {
                    alert('e')
                }
            });
            e.preventDefault();
        }
    });
        });
      </script>

</section>
@endsection
