@extends('layouts.review')

@section('content')
<section class="content-header">
      <h1>
        Review
        <small>Papers</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Papers</li>
        <li class="active">delails</li>
      </ol>
</section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
     <div class="row">
           <div class="col-xs-12">
             <div class="box box-info">
               <div class="box-header">
                 @isset($paper)
                 <div class="container">
                 <div class="row">
                     <div class="col-sm-4">
                      <b>Name</b> : {{$paper->author->fname}} &nbsp {{$paper->author->fname}}
                     </div>
                     <div class="col-sm-4">
                      <b>Organization</b> : {{$paper->author->organization}}
                     </div>
                     <div class="col-sm-4">
                      <b>Phone</b> : {{$paper->author->phone}}
                     </div>
                </div>
              </div>
                  @endisset
              </div>

            </div>
          </div>
      </div>
                <div class="row">
                  <div class="col-xs-8">
                    <div class="box box-info">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Title</label>
                          <input type="text" class="form-control" value="{{$paper->title}}" readonly="readonly">

                          </input>
                        </div>
                        <div class="form-group">
                          <label>Abstract</label>
                          <textarea class="form-control" rows="10" readonly="readonly">
                            {{$paper->abstract}}
                          </textarea>
                        </div>

                        <div class="form-group">
                          <label>Keywords</label>
                          <input type="text" class="form-control" value="{{$paper->keywords}}" readonly="readonly">
                        </div>

                        <div class="form-group">
                          <label>Paper Download</label>
                          <a href="{{asset('files/abstracts/'.$paper->file_name)}}" class="form-control" >{{$paper->file_name}}</a>
                        </div>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <div class="col-md-4">
                      <!-- category and image -->
                      <div class="box box-info">
                          <div class="box-header">
                              <h3 class="box-title">Authors</h3>
                          </div>
                          <div class="box-body pad">
                            <table class="table table-hover">
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                              </tr>
                              @foreach($authors as $author)
                              @isset($author)
                              <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$author->fname}}</td>
                                <td><a href="#" ><span class="label label-success">View</span></a></td>
                              </tr>
                              @endisset
                              @endforeach
                            </table>
                          </div>
                      </div>

                      <div class="box box-info">
                          <div class="box-header">
                              <h3 class="box-title">Actions</h3>
                          </div>
                          <div class="box-body pad">
                           <a href="/submit_review_page/{{$paper->id}}"<button type="button" class="btn btn-block btn-primary"><i class="fa fa-eye"></i>
                                Submit Review</button> </a>
                          </div>
                      </div>
                  </div>

                </div>
  </section>


@endsection
