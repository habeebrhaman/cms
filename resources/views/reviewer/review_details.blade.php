@extends('layouts.review')

@section('content')
<section class="content-header">
      <h1>
        Review
        <small>Submit</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Papers</li>
        <li>Delails</li>
        <li class="active">Submit</li>
      </ol>
</section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
     <div class="row">
           <div class="col-xs-12">
          </div>
      </div>
      <form action="/paper_review/{{$row->id}}" method="POST" id="CategoryCreate" enctype="multipart/form-data">
                 @method('PATCH')
                 @csrf
                <div class="row">
                  <div class="col-xs-8">
                    <div class="box box-info">
                      <div class="box-header">
                          <h3 class="box-title">Submit Review</h3>
                      </div>
                      <div class="box-body">


                        <input type="text" name="paper_id" hidden value="{{$row->paper_id}}">
                        <input type="text" name="conf_rev_id" hidden value="{{$conf_rev_id->id}}">

                        <div class="form-group">
                          <label>Status</label>
                          <select name="status_id" id="status_id" class="form-control">
                              <option value="0" {{$row->status == 0 ? "selected":"" }}>Strong Accept</option>
                              <option value="1" {{$row->status == 1 ? "selected":"" }}>Accept</option>
                              <option value="2" {{$row->status == 2 ? "selected":"" }}>Reject</option>
                              <option value="3" {{$row->status == 3 ? "selected":"" }}>Strong Reject</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Review Commands</label>
                          <textarea class="form-control" rows="10" name="commands">
                             {{$row->commands}}
                          </textarea>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-eye"></i>
                            Update Review</button>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                </div>
              </form>
  </section>


@endsection
