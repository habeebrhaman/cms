@extends('layouts.review')

@section('content')
<section class="content-header">
      <h1>
        Review
        <small>Add Topics</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Topics</li>
      </ol>
</section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
    <form action="/add_topics" method="POST" id="CategoryCreate" enctype="multipart/form-data">
               @csrf

            <div class="row">
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">Select Topics</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body pad">
                          <div class="form-group">
                              <label for="topic_id">Topic</label>
                              <select name="topic_id" id="topic_id" class="form-control">
                                  @foreach ($Topics as $x=>$Topics)
                                  <option value="{{ $Topics->id }}">{{ $Topics->topic_name }}</option>
                                  @endforeach
                              </select>

                          </div>
                            <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-eye"></i>
                                Add</button>

                        </div>
                    </div>
                </div>

                <div class="col-xs-8">
                  <div class="box box-info">
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped data-table">
                        <thead>
                          <tr>
                            <th class="col-sm-1">#</th>
                            <th>Topic</th>
                            <th class="col-sm-2">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($rows as $row)

                          @isset($row)
                          <tr>
                            <td>{{$loop->iteration }}</td>
                            <td>{{ $row->topics->topic_name }}</td>
                            <td>
                              <button id = "delete" class="btn btn-danger btn-xs delete" data-id="{{ $row->id }}"><i class="fa fa-trash"
                                  aria-hidden="true"></i>
                              </button>
                            </td>
                            {{ $rows->links() }}
                          </tr>
                          @endisset
                          @endforeach

                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>

            </div>
        </form>
</section>
<script>

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });

    $(document).ready(function () {
        $('.data-table').dataTable();
    });

    $(".delete").click(function (e) {
         e.preventDefault();
        var id = $(this).data("id");
        var con = confirm("Are You Sure!");
        if (con == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/add_topics/" + id,
                type: 'DELETE',
                data: {
                    "id": id,
                },
                success: function (response) {
                    window.location = '/add_topics';
                },
                error: function (xhr) {
                    alert('e')
                }
            });
        }
    });
    </script>
@endsection
