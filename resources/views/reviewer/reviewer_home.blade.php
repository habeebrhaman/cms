@extends('layouts.review')

@section('content')
<section class="content-header">
      <h1>
         Reviewer Dashboard
      </h1>
      <div class="row">
          <div class="col-md-8">
              <div class="box box-info">
                  <div class="box-header">
                      <h3 class="box-title">Notifications</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body pad">
                    <ul>
                          @foreach ($nots as $not)
                          @isset($not)
                            <li><a href="@isset($note->links){{$not->links}}@endisset">{{$not->notification}}</a></li>
                          @endisset
                          @endforeach
                   </ul>
                  </div>
              </div>
          </div>

      </div>
</section>

@endsection
