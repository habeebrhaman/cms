@extends('layouts.review')

@section('content')
<section class="content-header">
      <h1>
        Review
        <small>Papers</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Review Papers</li>
      </ol>
</section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
     </div>
            <div class="row">
                <div class="col-xs-8">
                  <div class="box box-info">
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped data-table">
                        <thead>
                          <tr>
                            <th class="col-sm-1">#</th>
                            <th>Paper Title</th>
                            <th class="col-sm-2">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($rows as $row)

                          @isset($row)
                          <tr>
                            <td>{{$loop->iteration }}</td>
                            <td>{{$row->paper->title}}</td>
                            <td><a href="/review_paper_details/{{$row->paper->id}}" ><span class="label label-success">View Paper</span></a>
                              @if($row->rev_status > 0)<a href="/review_paper/{{$row->paper->id}}" ><span class="label label-success">Review</span></a>
                              @endif</td>
                          </tr>
                          @endisset
                          @endforeach

                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>

            </div>
</section>
@endsection
