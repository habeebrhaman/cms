@extends('layouts.app')

@section('content')
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <div class="row justify-content-center">
        <div class="col-md-8">
          <form method="POST" action="/final_paper_upload" aria-label="{{ __('CONFERENCE REGISTRATION') }}" enctype="multipart/form-data">
              @csrf
              @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
            <input type="text" name="conf_id" value="{{$conference->id}}" hidden>
                  <div class="card">
                      <div class="card-header">Upload Final Paper</div>
                        <div class="card-body">

                        <div class="form-group row">
                            <label for="final_paper" class="col-md-4 col-form-label text-md-right">{{ __('File (*)') }}</label>
                            <div class="col-md-6">
                                <input id="final_paper" type="file" class="form-control{{ $errors->has('final_paper') ? ' is-invalid' : '' }}" name="final_paper" value="{{ old('final_paper') }}" required autofocus>

                                @if ($errors->has('final_paper'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('final_paper') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                          <div class="form-group row mb-0">
                              <div class="col-md-6 offset-md-4">
                                  <button type="submit" class="btn btn-primary">
                                      {{ __('Submit') }}
                                  </button>
                              </div>
                          </div>
                          @if ($errors->any())
                          <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                          </ul>
                      @endif

                      @if(session()->has('jsAlert'))
                          <script>
                              alert({{ session()->get('jsAlert') }});
                          </script>
                      @endif
                        </div>


            </form>

    </div>
</div>

<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
    $(document).ready(function(){
        $("#btn_add").click(function(e){
            e.preventDefault();
            $("#authors_details").append("<p></p");
        });
    });
</script>
@endsection
