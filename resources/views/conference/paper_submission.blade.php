@extends('layouts.app')

@section('content')
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <div class="row justify-content-center">
        <div class="col-md-8">
          <form method="POST" action="/new_submission" aria-label="{{ __('CONFERENCE REGISTRATION') }}" enctype="multipart/form-data">
              @csrf
            <input type="text" name="conf_id" value="{{$conference->id}}" hidden>
            <div id="authors_details">
            <div class="card">
                <div class="card-header">Auther 1</div>
                  <div class="card-body">
                    <div class="form-group row">
                        <label for="fname1" class="col-md-4 col-form-label text-md-right">{{ __('First Name (*)') }}</label>
                        <div class="col-md-6">
                            <input id="fname1" type="text" class="form-control{{ $errors->has('fname1') ? ' is-invalid' : '' }}" name="fname1" value="{{ old('fname1') }}" required autofocus>

                            @if ($errors->has('fname1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('fname1') }}</strong>
                                </span>
                            @endif
                     </div>
                   </div>
                  <div class="form-group row">
                      <label for="lname1" class="col-md-4 col-form-label text-md-right">{{ __('LastName (*)') }}</label>
                      <div class="col-md-6">
                          <input id="lname1" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname1" value="{{ old('lname') }}" required autofocus>

                          @if ($errors->has('lname'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('lname') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="email1" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail (*)') }}</label>

                      <div class="col-md-6">
                          <input id="email1" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email1" value="{{ old('email') }}" required>

                          @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                <div class="form-group row">
                    <label for="org1" class="col-md-4 col-form-label text-md-right">{{ __('Organization (*)') }}</label>

                    <div class="col-md-6">
                        <input id="org1" type="text" class="form-control{{ $errors->has('org') ? ' is-invalid' : '' }}" name="org1" value="{{ old('org1') }}" required autofocus>

                        @if ($errors->has('org1'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('org1') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="check1" class="col-md-4 col-form-label text-md-right">{{ __('Curresponding Author') }}</label>

                    <div class="col-md-6">
                        <input id="check1" type="checkbox" name="check1">

                    </div>
                </div>
            </div>
          </div>
        </div>
        <button id="btn_add">Add Authors</button>
           <!-- <div class="card">
              <div class="card-header">Auther 2</div>
                <div class="card-body">
                  <div class="form-group row">
                      <label for="fname2" class="col-md-4 col-form-label text-md-right">{{ __('First Name (*)') }}</label>
                      <div class="col-md-6">
                          <input id="fname2" type="text" class="form-control{{ $errors->has('fname2') ? ' is-invalid' : '' }}" name="fname2" value="{{ old('fname2') }}"  autofocus>

                          @if ($errors->has('fname2'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('fname2') }}</strong>
                              </span>
                          @endif
                   </div>
                 </div>
                <div class="form-group row">
                    <label for="lname2" class="col-md-4 col-form-label text-md-right">{{ __('LastName (*)') }}</label>
                    <div class="col-md-6">
                        <input id="lname2" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname2" value="{{ old('lname') }}"  autofocus>

                        @if ($errors->has('lname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('lname') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email2" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail (*)') }}</label>

                    <div class="col-md-6">
                        <input id="email2" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email2" value="{{ old('email') }}" >

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
              <div class="form-group row">
                  <label for="org2" class="col-md-4 col-form-label text-md-right">{{ __('Organization (*)') }}</label>

                  <div class="col-md-6">
                      <input id="org2" type="text" class="form-control{{ $errors->has('org') ? ' is-invalid' : '' }}" name="org2" value="{{ old('org2') }}"  autofocus>

                      @if ($errors->has('org2'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('org2') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
              <div class="form-group row">
                  <label for="check2" class="col-md-4 col-form-label text-md-right">{{ __('Curresponding Author') }}</label>

                  <div class="col-md-6">
                      <input id="check2" type="checkbox" name="check">

                  </div>
              </div>
          </div>
        </div>
        <div class="card">
            <div class="card-header">Auther 3</div>
              <div class="card-body">
                <div class="form-group row">
                    <label for="fname3" class="col-md-4 col-form-label text-md-right">{{ __('First Name (*)') }}</label>
                    <div class="col-md-6">
                        <input id="fname3" type="text" class="form-control{{ $errors->has('fname3') ? ' is-invalid' : '' }}" name="fname3" value="{{ old('fname3') }}"  autofocus>

                        @if ($errors->has('fname3'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('fname3') }}</strong>
                            </span>
                        @endif
                 </div>
               </div>
              <div class="form-group row">
                  <label for="lname3" class="col-md-4 col-form-label text-md-right">{{ __('LastName (*)') }}</label>
                  <div class="col-md-6">
                      <input id="lname3" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname3" value="{{ old('lname') }}"  autofocus>

                      @if ($errors->has('lname'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('lname') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
              <div class="form-group row">
                  <label for="email3" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail (*)') }}</label>

                  <div class="col-md-6">
                      <input id="email3" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email3" value="{{ old('email') }}" >

                      @if ($errors->has('email'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
            <div class="form-group row">
                <label for="org3" class="col-md-4 col-form-label text-md-right">{{ __('Organization (*)') }}</label>

                <div class="col-md-6">
                    <input id="org3" type="text" class="form-control{{ $errors->has('org') ? ' is-invalid' : '' }}" name="org3" value="{{ old('org3') }}"  autofocus>

                    @if ($errors->has('org3'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('org3') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="check3" class="col-md-4 col-form-label text-md-right">{{ __('Curresponding Author') }}</label>

                <div class="col-md-6">
                    <input id="check3" type="checkbox" name="check">

                </div>
            </div>
        </div>
      </div>
      <div class="card">
          <div class="card-header">Auther 4</div>
            <div class="card-body">
              <div class="form-group row">
                  <label for="fname4" class="col-md-4 col-form-label text-md-right">{{ __('First Name (*)') }}</label>
                  <div class="col-md-6">
                      <input id="fname4" type="text" class="form-control{{ $errors->has('fname4') ? ' is-invalid' : '' }}" name="fname4" value="{{ old('fname4') }}"  autofocus>

                      @if ($errors->has('fname4'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('fname4') }}</strong>
                          </span>
                      @endif
               </div>
             </div>
            <div class="form-group row">
                <label for="lname4" class="col-md-4 col-form-label text-md-right">{{ __('LastName (*)') }}</label>
                <div class="col-md-6">
                    <input id="lname4" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname4" value="{{ old('lname') }}"  autofocus>

                    @if ($errors->has('lname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('lname') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="email4" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail (*)') }}</label>

                <div class="col-md-6">
                    <input id="email4" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email4" value="{{ old('email') }}" >

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
          <div class="form-group row">
              <label for="org4" class="col-md-4 col-form-label text-md-right">{{ __('Organization (*)') }}</label>

              <div class="col-md-6">
                  <input id="org4" type="text" class="form-control{{ $errors->has('org') ? ' is-invalid' : '' }}" name="org4" value="{{ old('org4') }}"  autofocus>

                  @if ($errors->has('org4'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('org4') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
          <div class="form-group row">
              <label for="check4" class="col-md-4 col-form-label text-md-right">{{ __('Curresponding Author') }}</label>

              <div class="col-md-6">
                  <input id="check4" type="checkbox" name="check">

              </div>
          </div>
      </div>
    </div>
    <div class="card">
        <div class="card-header">Auther 5</div>
          <div class="card-body">
            <div class="form-group row">
                <label for="fname5" class="col-md-4 col-form-label text-md-right">{{ __('First Name (*)') }}</label>
                <div class="col-md-6">
                    <input id="fname5" type="text" class="form-control{{ $errors->has('fname5') ? ' is-invalid' : '' }}" name="fname5" value="{{ old('fname5') }}"  autofocus>

                    @if ($errors->has('fname5'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fname5') }}</strong>
                        </span>
                    @endif
             </div>
           </div>
          <div class="form-group row">
              <label for="lname5" class="col-md-4 col-form-label text-md-right">{{ __('LastName (*)') }}</label>
              <div class="col-md-6">
                  <input id="lname5" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname5" value="{{ old('lname') }}"  autofocus>

                  @if ($errors->has('lname'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('lname') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
          <div class="form-group row">
              <label for="email5" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail (*)') }}</label>

              <div class="col-md-6">
                  <input id="email5" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email5" value="{{ old('email') }}" >

                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
        <div class="form-group row">
            <label for="org5" class="col-md-4 col-form-label text-md-right">{{ __('Organization (*)') }}</label>

            <div class="col-md-6">
                <input id="org5" type="text" class="form-control{{ $errors->has('org') ? ' is-invalid' : '' }}" name="org5" value="{{ old('org5') }}"  autofocus>

                @if ($errors->has('org5'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('org5') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="check5" class="col-md-4 col-form-label text-md-right">{{ __('Curresponding Author') }}</label>

            <div class="col-md-6">
                <input id="check5" type="checkbox" name="check">

            </div>
        </div>
    </div>
  </div> -->
          <hr class="m-y-md">

      <div class="card">
          <div class="card-header">Title and Abstract</div>
            <div class="card-body">

            <div class="form-group row">
                <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title (*)') }}</label>
                <div class="col-md-6">
                    <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                    @if ($errors->has('title'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="abstract" class="col-md-4 col-form-label text-md-right">{{ __('Abstract (*)') }}</label>
                <div class="col-md-6">
                    <textarea rows="20" cols="50" name="abstract" id="abstract" required>
                    </textarea>
                    @if ($errors->has('area'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('area') }}</strong>
                        </span>
                    @endif
             </div>
           </div>

              </div>
            </div>
            <hr class="m-y-md">
            <div class="card">
                <div class="card-header">KeyWords</div>
                  <div class="card-body">
                  <div class="form-group row">
                      <label for="key" class="col-md-4 col-form-label text-md-right">{{ __('Keywords (*)') }}</label>
                      <div class="col-md-6">
                          <input id="key" type="text" class="form-control{{ $errors->has('key') ? ' is-invalid' : '' }}" name="key" value="{{ old('key') }}"  required autofocus>

                          @if ($errors->has('key'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('key') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                    </div>
                  </div>
                  <hr class="m-y-md">
                  <div class="card">
                      <div class="card-header">File Upload</div>
                        <div class="card-body">

                        <div class="form-group row">
                            <label for="abstract_file" class="col-md-4 col-form-label text-md-right">{{ __('File (*)') }}</label>
                            <div class="col-md-6">
                                <input id="abstract_file" type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="abstract_file" value="{{ old('file') }}" required autofocus>

                                @if ($errors->has('file'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                          <div class="form-group row mb-0">
                              <div class="col-md-6 offset-md-4">
                                  <button type="submit" class="btn btn-primary">
                                      {{ __('Submit') }}
                                  </button>
                              </div>
                          </div>
                          @if ($errors->any())
                          <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                          </ul>
                      @endif

                      @if(session()->has('jsAlert'))
                          <script>
                              alert({{ session()->get('jsAlert') }});
                          </script>
                      @endif
                        </div>


            </form>

    </div>
</div>

<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
    $(document).ready(function(){
        $("#btn_add").click(function(e){
            e.preventDefault();
            $("#authors_details").append("<p></p");
        });
    });
</script>
@endsection
