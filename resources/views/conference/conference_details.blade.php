@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Conferences
        <small>Call For Paper</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Call For Paper</li>
      </ol>
    </section>
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert"
                aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

          <form action="/conference/{{$conf->id}}" method="POST" id="chair_create" enctype="multipart/form-data">
          @method('PUT')
            @csrf
              <div class="row">
                  <div class="col-md-8">
                      <!-- category and image -->
                      <div class="box box-info">
                          <div class="box-header">

                              <h3 class="box-title">Update</h3>

                          </div>

                          <div class="box-body pad">

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$conf->name}}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="acronym" class="col-md-4 col-form-label text-md-right">{{ __('Acronym') }}</label>

                                <div class="col-md-6">
                                    <input id="acronym" type="text" class="form-control{{ $errors->has('acronym') ? ' is-invalid' : '' }}" name="acronym" value="{{$conf->acronym }}" required autofocus>

                                    @if ($errors->has('acronym'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('acronym') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="web" class="col-md-4 col-form-label text-md-right">{{ __('Web') }}</label>

                                <div class="col-md-6">
                                    <input id="web" type="text" class="form-control{{ $errors->has('web') ? ' is-invalid' : '' }}" name="web" value="{{$conf->web}}" required autofocus>

                                    @if ($errors->has('web'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('web') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$conf->email}}" readonly>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="venue" class="col-md-4 col-form-label text-md-right">{{ __('venue') }}</label>

                                <div class="col-md-6">
                                    <input id="venue" type="text" class="form-control{{ $errors->has('venue') ? ' is-invalid' : '' }}" name="venue" value="{{$conf->venue}}" required autofocus>

                                    @if ($errors->has('venue'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('venue') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                                <div class="col-md-6">
                                    <input id="affliation" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{$conf->city}}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                                <div class="col-md-6">
                                    <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{$conf->country}}" required autofocus>

                                    @if ($errors->has('country'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="startdate" class="col-md-4 col-form-label text-md-right">{{ __('Start Date') }}</label>

                                <div class="col-md-6">
                                    <input id="startdate" type="date" class="form-control{{ $errors->has('startdate') ? ' is-invalid' : '' }}" name="startdate" value="{{$conf->startdate}}" required>

                                    @if ($errors->has('startdate'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('startdate') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="enddate" class="col-md-4 col-form-label text-md-right">{{ __('End Date') }}</label>

                                <div class="col-md-6">
                                    <input id="enddate" type="date" class="form-control{{ $errors->has('enddate') ? ' is-invalid' : '' }}" name="enddate" value="{{$conf->enddate}}" required>

                                    @if ($errors->has('enddate'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('enddate') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="year" class="col-md-4 col-form-label text-md-right">{{ __('Year') }}</label>

                                <div class="col-md-6">
                                    <input id="year" type="text" class="form-control{{ $errors->has('year') ? ' is-invalid' : '' }}" value="{{$conf->year}}" name="year" required>

                                    @if ($errors->has('year'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="about" class="col-md-4 col-form-label text-md-right">{{ __('About') }}</label>

                                <div class="col-md-6">
                                    <textarea id="about" class="form-control" name="about">
                                      {{$conf->about}}
                                    </textarea>


                                </div>
                            </div>

                             <div class="col-4 pull-right">

                                 <button type="submit" class="btn btn-block btn-primary store"><i class="fa fa-eye"></i>
                            Update </button>
                             </div>
                          </div>
                      </div>
                  </div>
                  </div>

       </form>

        <script>
        $(document).ready(function(){
          $(".delete").click(function (e) {

        var id = $(this).data("id");
        var con = confirm("Are You Sure!");
        if (con == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/chair/" + id,
                type: 'DELETE',
                data: {
                    "id": id,
                },
                success: function (response) {
                     location.reload();
                },
                error: function (xhr) {
                    alert('e')
                }
            });
            e.preventDefault();
        }
    });
        });
      </script>

</section>
@endsection
