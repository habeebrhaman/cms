@extends('layouts.conference')
@section('content')
<div class="row">
  <div class="col-sm-8"  style="text-align:center;">
    <h4>About Conference</h4>
    <p>{{$conference->about}}</p>
  </div>
  <div class="col-sm-4"><img src="{{asset('images/confimage.jpg')}}" height="200px">
  </div>
  <div>
    <div class="panel panel-info">
          <div class="panel-heading">
              <h3 class="panel-title">Conference Areas</h3>
          </div>
          <div class="panel-body">
          <ul>
            <li>{{$areas->topic1->topic_name}}</li>
            <li>{{$areas->topic2->topic_name}}</li>
          </ul>
          </div>
    </div>
  <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Importent Dates</h3>
        </div>
        <div class="panel-body">
        <ul>
          <li>Submission Date : {{$conference->callForPaper->submission_date}}</li>
          <li>End Date : {{$conference->callForPaper->end_date}}</li>
        </ul>
        </div>
  </div>

  <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Conference Chair</h3>
        </div>
        <div class="panel-body">
          <ul>
           @foreach($conference->chair as $ch)
           @isset($ch)
           <li>{{$ch->name}}</li>
           @endisset
           @endforeach
          </ul>
        </div>
  </div>

  <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Technical Committee</h3>
        </div>
        <div class="panel-body">
          <ul>
           @foreach($conference->technical as $tech)
           @isset($tech)
           <li>{{$tech->name}}</li>
           @endisset
           @endforeach
          </ul>
        </div>
  </div>

  <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Advisory Committee</h3>
        </div>
        <div class="panel-body">
          <ul>
           @foreach($conference->advisory as $ad)
           @isset($ad)
           <li>{{$ad->name}}</li>
           @endisset
           @endforeach
          </ul>
        </div>
  </div>
@endsection
