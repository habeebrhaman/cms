@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4><b>Hi, Welcome {{ Auth::user()->fname }}</b></h4></div>
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <div class="card-body">
                  @if(isset($papers))
                    <ul>
                      <lh><b>Your papers</b></lh>
                        @foreach ($papers as $paper)
                          @isset($paper)
                          <li><a href="/update/{{$paper->id}}">{{$paper->title}}</a></li>
                          @endisset
                        @endforeach
                    </ul>
                  @else
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
