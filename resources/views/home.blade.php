@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4><b>Hi, Welcome {{ Auth::user()->fname }}</b></h4></div>
                @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <div class="card-body">
                  @if(isset($conference))
                    <ul>
                      <lh><b>Your Conferences</b></lh>
                        @foreach ($conference as $conf)
                          @isset($conf)
                          <li><a href="/conferences/{{$conf->id}}">{{$conf->conferences->name}}</a></li>
                          @endisset
                        @endforeach
                    </ul>
                    <ul>
                        <lh><b>Notifications</b></lh>
                          @foreach ($nots as $not)
                          @isset($not)
                            <li><a href="{{$not->links}}">{{$not->notification}}</a></li>
                          @endisset
                          @endforeach
                   </ul>
                  @else
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
