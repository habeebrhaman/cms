<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_mails', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('conference_rev_id');
            $table->boolean('accept_mail_send')->default(0);
            $table->boolean('final_paper_mail_send')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_mails');
    }
}
