<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallForPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_for_papers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('end_date')->nullable();
            $table->date('submission_date')->nullable();
            $table->date('extended_date')->nullable();
            $table->unsignedInteger('primary_areas_id');
            $table->unsignedInteger('secondary_areas_id');
            $table->string('area_note')->nullable();
            $table->unsignedInteger('conferences_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_for_papers');
    }
}
