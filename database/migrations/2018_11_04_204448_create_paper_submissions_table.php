<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paper_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('auther_id');
            $table->unsignedInteger('conferences_id');
            $table->longText('title')->nullable();
            $table->longText('abstract')->nullable();
            $table->string('keywords')->nullable();
            $table->string('file_name');
            $table->smallInteger('review_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_submissions');
    }
}
