<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConferenceReviewersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conference_reviewers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('conference_id');
            $table->unsignedInteger('reviewer_id');
            $table->unsignedInteger('paper_id');
            $table->string('paper_generated_id');
            $table->smallInteger('status')->default(0);
            $table->smallInteger('rev_status')->default(0);
            $table->boolean('is_inactive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conference_reviewers');
    }
}
