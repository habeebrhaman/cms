<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ConferenceReviewer;

class ConferenceReviewer extends Model
{
  public function reviewer(){
    return $this->belongsTo('App\User','reviewer_id');
  }

  public function conferences(){
    return $this->belongsTo('App\Conference','conference_id');
  }

  public function paper(){
    return $this->belongsTo('App\PaperSubmission','paper_id');
  }

  public function mailSender(){
    return $this->hasOne('App\PaperMail','conference_rev_id');
  }

  public function reviews(){
    return $this->hasOne('App\Review','conference_rev_id');
  }
}
