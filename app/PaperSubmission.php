<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaperSubmission extends Model
{
  public function author(){
    return $this->belongsTo('App\User','auther_id');
  }

  public function conferences(){
    return $this->belongsTo('App\Conference','conferences_id');
  }
}
