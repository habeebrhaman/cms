<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicalCommittee extends Model
{
    protected $fillable = [
        'name','conferences_id'
    ];
  
    public $table ='technical_committees';
}
