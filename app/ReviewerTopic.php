<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewerTopic extends Model
{
  public function topics()
  {
    return $this->belongsTo('App\TopicArea', 'topic_id');
  }
}
