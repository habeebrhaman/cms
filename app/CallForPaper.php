<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallForPaper extends Model
{
    protected $fillable = [
        'end_date','submission_date','extended_date','primary_areas_id','secondary_areas_id','conferences_id'
    ];

    public $table ='call_for_papers';

    public function topic1()
    {
      return $this->belongsTo('App\TopicArea', 'primary_areas_id');
    }

    public function topic2()
    {
      return $this->belongsTo('App\TopicArea', 'secondary_areas_id');
    }
}
