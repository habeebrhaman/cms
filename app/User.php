<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname','lname','organization','affliation','country','web-url','phone','is_reviewer' ,'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cfp(){
      return $this->hasOne('App\PaperSubmission','user_id');
    }
    public function is_reviewer()
    {
      if($this->is_reviewer)
      {
        return true;
      }
      else{
        return false;
      }
    }
}
