<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvisoryCommittee extends Model
{
    protected $fillable = [
        'name','conferences_id'
    ];
  
    public $table ='advisory_committees';
}
