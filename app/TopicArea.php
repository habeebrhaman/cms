<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicArea extends Model
{
  public function parent()
  {
    return $this->belongsTo('App\TopicArea', 'parent_id', 'id');
  }

  public function subTopics()
  {
    return $this->hasMany('App\TopicArea', 'parent_id', 'id');
  }
}
