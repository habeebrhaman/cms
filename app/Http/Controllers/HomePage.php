<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conference;
class HomePage extends Controller
{
    public function index()
    {
      return view('home-page')->with([
        'conference' => Conference::orderBy('id','DESC')->get(),
      ]);

    }
}
