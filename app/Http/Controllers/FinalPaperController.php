<?php

namespace App\Http\Controllers;

use App\FinalPaper;
use Illuminate\Http\Request;
use App\PaperSubmission;
use App\Notification;
use App\ConferenceReviewer;


class FinalPaperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user_id = auth()->user()->id;
      $duplicate = FinalPaper::where('user_id',$user_id)->get();
      $count = $duplicate->count();

      if($count >0)
      {
        return redirect()->back()->with('message','already submitted');
      }
      else
      {
      $conf_id = PaperSubmission::where('user_id',$user_id)->first();
      $paper_id = ConferenceReviewer::where([['paper_id',$conf_id->id],['is_inactive',0]])->pluck('paper_generated_id')->first();
        
        $store = new FinalPaper();
        $store->final_id = "FINAL-".$paper_id;
        $store->conference_id = $conf_id->conferences_id;
        $store->user_id = $user_id;
        if ($request->has('final_paper')) {
          $name2 = $request->final_paper->getClientOriginalName();
          $request->final_paper->move(public_path().'/files/final/', $name2);
          $img2 = $name2;
          $store->filename = $img2;
        }

        $store->save();

        return redirect()->back()->with('message','Paper submitted successfully');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FinalPaper  $finalPaper
     * @return \Illuminate\Http\Response
     */
    public function show(FinalPaper $finalPaper)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FinalPaper  $finalPaper
     * @return \Illuminate\Http\Response
     */
    public function edit(FinalPaper $finalPaper)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FinalPaper  $finalPaper
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinalPaper $finalPaper)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FinalPaper  $finalPaper
     * @return \Illuminate\Http\Response
     */
    public function destroy(FinalPaper $finalPaper)
    {
        //
    }
}
