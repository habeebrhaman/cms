<?php

namespace App\Http\Controllers;

use App\TopicArea;
use Illuminate\Http\Request;

class TopicAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
      return view('topic_area.create')->with([
          'rows' => TopicArea::with('parent')->paginate(10),
          'Topics' => TopicArea::where('parent_id',0)->get(),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('topic_area.create')->with([
           'Topics' => TopicArea::where('parent_id',0)->get(),
           'rows' => TopicArea::with('parent')->paginate(10),
           ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new TopicArea();
        $add->topic_name = $request->topic_name;
        $add->parent_id = $request->parent_id;

        $add->save();

        return view('topic_area.create')->with([
             'Topics' => TopicArea::where('parent_id',0)->get(),
             'rows' => TopicArea::with('parent')->paginate(10),
             ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TopicArea  $topicArea
     * @return \Illuminate\Http\Response
     */
    public function show(TopicArea $topicArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TopicArea  $topicArea
     * @return \Illuminate\Http\Response
     */
    public function edit(TopicArea $topicArea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TopicArea  $topicArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TopicArea $topicArea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TopicArea  $topicArea
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ss = TopicArea::find($id);
      $ss->delete();
      return view('topic_area.create')->with([
           'Topics' => TopicArea::where('parent_id',0)->get(),
           'rows' => TopicArea::with('parent')->paginate(10),
           ]);

    }
}
