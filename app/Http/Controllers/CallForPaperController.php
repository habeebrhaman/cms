<?php

namespace App\Http\Controllers;

use App\CallForPaper;
use Illuminate\Http\Request;
use Auth;
use App\TopicArea;
use App\Conference;

class CallForPaperController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:editorlogin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        $count = CallForPaper::where('conferences_id',$conference_id)->get();
        if($count->count() > 0)
        {
          return view('editorial-manager.cfp.call_for_paper')->with([
              'row' => CallForPaper::where('conferences_id',$conference_id)->with('topic1')->with('topic2')->first(),
              'Topics' => TopicArea::where('parent_id',0)->get(),
              'update' => 1,
              'id' => CallForPaper::orderBy('id','DESC')->where('conferences_id',$conference_id)->first(),
             ]);
        }
        else {
          return view('editorial-manager.cfp.call_for_paper')->with([
              'row' => CallForPaper::where('conferences_id',$conference_id)->with('topic1')->with('topic2')->first(),
              'Topics' => TopicArea::where('parent_id',0)->get(),
              'update' => 0,
             ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        $cfp = new CallForPaper;
        $cfp->end_date = $request->e_date;
        $cfp->submission_date = $request->s_date;
        $cfp->extended_date = null;
        $cfp->primary_areas_id = $request->primary_id;
        $cfp->secondary_areas_id = $request->secondary_id;
        $cfp->area_note = $request->notes;
        $cfp->conferences_id = $conference_id;

        $cfp->save();
        return view('editorial-manager.cfp.call_for_paper')->with([
            'row' => CallForPaper::where('conferences_id',$conference_id)->with('topic1')->with('topic2')->first(),
            'Topics' => TopicArea::where('parent_id',0)->get(),
            'update' => 1,
            'id' => CallForPaper::orderBy('id','DESC')->where('conferences_id',$conference_id)->first(),
           ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CallForPaper  $callForPaper
     * @return \Illuminate\Http\Response
     */
    public function show(CallForPaper $callForPaper)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CallForPaper  $callForPaper
     * @return \Illuminate\Http\Response
     */
    public function edit(CallForPaper $callForPaper)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CallForPaper  $callForPaper
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CallForPaper $callForPaper)
    {
      $email = Auth::user()->email;
      $conference_id = Conference::where('email' ,$email)->pluck('id')->first();

      if($request->has('e_date'))
      {
        $callForPaper->end_date = $request->e_date;
      }
      if($request->has('s_date'))
      {
        $callForPaper->submission_date = $request->s_date;
      }
      if($request->has('ex_date'))
      {
      $callForPaper->extended_date = $request->ex_date;
       }
      if($request->has('primary_id'))
      {
      $callForPaper->primary_areas_id = $request->primary_id;
       }
      if($request->has('secondary_id'))
      {
      $callForPaper->secondary_areas_id = $request->secondary_id;
       }
      if($request->has('notes'))
      {
      $callForPaper->area_note = $request->notes;
       }
      $callForPaper->conferences_id = $conference_id;
    

      $callForPaper->save();
      return view('editorial-manager.cfp.call_for_paper')->with([
          'row' => CallForPaper::where('conferences_id',$conference_id)->with('topic1')->with('topic2')->first(),
          'Topics' => TopicArea::where('parent_id',0)->get(),
          'update' => 1,
          'id' => CallForPaper::orderBy('id','DESC')->where('conferences_id',$conference_id)->first(),
         ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CallForPaper  $callForPaper
     * @return \Illuminate\Http\Response
     */
    public function destroy(CallForPaper $callForPaper)
    {
        //
    }
}
