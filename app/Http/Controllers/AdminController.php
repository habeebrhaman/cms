<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conference;
use App\EditorLogin;
use Illuminate\Support\Facades\Hash;
use Mail;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.admin');
    }

    public function getNew()
    {
      return view('admin.new_conferences')->with([
        'rows' => Conference::where('is_published',0)->get(),
      ]);
    }

    public function acceptConf($id)
    {
      $email = Conference::where('id',$id)->pluck('email')->first();
      $myStr = str_random(8);
      $accept = new EditorLogin();

      $accept->conferences_id = $id;
      $accept->email = $email;
      $accept->password = Hash::make($myStr);

      $accept->save();

      $conf = Conference::find($id);

      $conf->is_published = 1;

      $conf->save();

      Mail::send('email',
       array(
           'email' => $email,
           'password' => $myStr,
       ),
       function($message) use($email)
       {
           $message->from('habeeb.pth@outlook.com');
           $message->to($email, 'Admin')->subject('Editorial Manager Conference Registration');

       });

      return view('admin.new_conferences')->with([
        'rows' => Conference::where('is_published',0)->get(),
      ]);

    }

}
