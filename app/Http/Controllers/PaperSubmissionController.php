<?php

namespace App\Http\Controllers;

use App\PaperSubmission;
use Illuminate\Http\Request;
use App\Conference;
use App\Author;
class PaperSubmissionController extends Controller
{
     public $conf_id;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id = auth()->user()->id;
        $new_sub = new PaperSubmission();

        $new_sub->auther_id = $user_id;
        $new_sub->user_id = $user_id;
        $new_sub->paper_id = "10-PAPER";
        $new_sub->title = $request->title;
        $new_sub->abstract = $request->abstract;
        $new_sub->keywords = $request->key;
        if ($request->has('abstract_file')) {
          $name2 = $request->abstract_file->getClientOriginalName();
          $request->abstract_file->move(public_path().'/files/abstracts/', $name2);
          $img2 = $name2;
          $new_sub->file_name = $img2;
        }
        $new_sub->conferences_id = $request->conf_id;
        $new_sub->save();


          $author1 = new Author();
          $author1->fname = $request->fname1;
          $author1->lname = $request->lname1;
          $author1->email = $request->email1;
          $author1->organization = $request->org1;
          $request->has('check1') ? $author1->is_curresponding = 1:$author1->is_curresponding = 0;
          $author1->paper_submission_id = $new_sub->id;
          $author1->save();
        //   if($request->has('fname2'))
        //   {
        //   $author2 = new Author();
        //   $author2->fname = $request->fname2;
        //   $author2->lname = $request->lname2;
        //   $author2->email = $request->email2;
        //   $author2->organization = $request->org2;
        //   $request->has('check2') ? $author2->is_curresponding = 1:$author2->is_curresponding = 0;
        //   $author2->paper_submission_id = $new_sub->id;
        //   $author2->save();
        //   }
        //   if($request->has('fname3'))
        //   {
        //   $author3 = new Author();
        //   $author3->fname = $request->fname3;
        //   $author3->lname = $request->lname3;
        //   $author3->email = $request->email3;
        //   $author3->organization = $request->org3;
        //   $request->has('check3') ? $author3->is_curresponding = 1:$author3->is_curresponding = 0;
        //   $author3->paper_submission_id = $new_sub->id;
        //   $author3->save();
        // }
        //   if($request->has('fname4'))
        //   {
        //   $author4 = new Author();
        //   $author4->fname = $request->fname4;
        //   $author4->lname = $request->lname4;
        //   $author4->email = $request->email4;
        //   $author4->organization = $request->org4;
        //   $request->has('check4') ? $author4->is_curresponding = 1:$author4->is_curresponding = 0;
        //   $author4->paper_submission_id = $new_sub->id;
        //   $author4->save();
        // }
        //   if($request->has('fname5'))
        //   {
        //   $author5 = new Author();
        //   $author5->fname = $request->fname5;
        //   $author5->lname = $request->lname5;
        //   $author5->email = $request->email5;
        //   $author5->organization = $request->org5;
        //   $request->has('check5') ? $author5->is_curresponding = 1:$author5->is_curresponding = 0;
        //   $author4->paper_submission_id = $new_sub->id;
        //   $author5->save();
        // }


        return view('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaperSubmission  $paperSubmission
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user_id = auth()->user()->id;
      $conf_id = Conference::where('id',$id)->pluck('id')->first();
      $exist = PaperSubmission::where('auther_id',$user_id)->pluck('auther_id')->first();
      if($exist == $user_id)
      {
        return view('home')->with([
          'conference' => Conference::where('id',$id)->first(),
        ]);
      }
      else{

      }
      return view('conference.paper_submission')->with([
        'conference' => Conference::where('id',$id)->first(),
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaperSubmission  $paperSubmission
     * @return \Illuminate\Http\Response
     */
    public function edit(PaperSubmission $paperSubmission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaperSubmission  $paperSubmission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaperSubmission $new_sub)
    {
      $new_sub->title = $request->title;
      $new_sub->abstract = $request->abstract;
      $new_sub->keywords = $request->key;
      if ($request->has('abstract_file')) {
        $name2 = $request->abstract_file->getClientOriginalName();
        $request->abstract_file->move(public_path().'/files/abstracts/', $name2);
        $img2 = $name2;
        $new_sub->file_name = $img2;
      }

      $new_sub->save();
      return view('update_submissions')->with([
        "paper" => PaperSubmission::where('id',$new_sub->id)->first(),
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaperSubmission  $paperSubmission
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaperSubmission $paperSubmission)
    {
        //
    }
}
