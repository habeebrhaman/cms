<?php

namespace App\Http\Controllers;

use App\TechnicalCommittee;
use Illuminate\Http\Request;
use Auth;
use App\Conference;

class TechnicalCommitteeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:editorlogin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        return view('editorial-manager.technical.technical')->with([
            'rows' => TechnicalCommittee::where('conferences_id',$conference_id)->get(),

           ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        $tech = new TechnicalCommittee;
        $tech->name = $request->name;
        $tech->conferences_id = $conference_id;
        $tech->save();
        return redirect()->back()->with([
            'rows' => TechnicalCommittee::where('conferences_id',$conference_id)->get(),
            ]);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TechnicalCommittee  $technicalCommittee
     * @return \Illuminate\Http\Response
     */
    public function show(TechnicalCommittee $technicalCommittee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TechnicalCommittee  $technicalCommittee
     * @return \Illuminate\Http\Response
     */
    public function edit(TechnicalCommittee $technicalCommittee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TechnicalCommittee  $technicalCommittee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TechnicalCommittee $technicalCommittee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TechnicalCommittee  $technicalCommittee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tech = TechnicalCommittee::findOrFail($id);
        $tech->delete();
    }
}
