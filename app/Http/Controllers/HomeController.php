<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaperSubmission;
use App\Notification;
use App\ConferenceReviewer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user_id = auth()->user()->id;
        return view('home')->with([
          "conference" => PaperSubmission::where('user_id',$user_id)->with('conferences')->get(),
          "nots" => Notification::orderBy('id','DESC')->where('is_authors',1)->orWhere([['is_authors',0],['is_reviewers',0]])->get(),
        ]);
    }
    public function reviewIndex()
    {

        return view('reviewer.reviewer_home')->with([
          "nots" => Notification::orderBy('id','DESC')->where('is_reviewers',1)->orWhere([['is_authors',0],['is_reviewers',0]])->get(),
        ]);

    }
    public function showFinalPage()
    {
      $user_id = auth()->user()->id;
      $paper_id = PaperSubmission::where('user_id',$user_id)->first();
      $status = ConferenceReviewer::orderBy('id','DESC')->where([['status',2],['is_inactive',0],['paper_id',$paper_id]])->with('paper')->whereHas('paper', function ($query) {
          $query->where('review_status',0)->orWhere('review_status',1);
      })->get();

      if($status->count() > 0)
      {
        return redirect()->back()->with('message','not aligible');
      }
      else {
        return view('conference.final_paper')->with([
          "conference" => PaperSubmission::where('user_id',$user_id)->first(),
        ]);
      }

    }
    public function showSubmissions()
    {
      $user_id = auth()->user()->id;
      return view('home_submissions')->with([
        "papers" => PaperSubmission::where('user_id',$user_id)->with('conferences')->get(),
      ]);
    }

    public function updatePage($id)
    {
      return view('update_submissions')->with([
        "paper" => PaperSubmission::where('id',$id)->first(),
      ]);
    }


}
