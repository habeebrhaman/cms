<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conference;
use App\CallForPaper;

class ConferencePage extends Controller
{
    public function showConference($id)
    {
        return view('conference.conference')->with([

        'conference' => Conference::where('id',$id)->with('chair')->with('technical')->with('advisory')->with('callForPaper')->first(),
        'areas' => CallForPaper::where('conferences_id',$id)->with('topic1')->with('topic2')->first(),
      ]);
    }
}
