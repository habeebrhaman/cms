<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\PaperSubmission;
use App\Conference;
use App\Author;
class SubmittedPaperController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:editorlogin');
  }

  public function index()
  {
    $email = Auth::user()->email;
    $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
    return view('editorial-manager.paper_submissions.submitted_papers')->with([
      'papers' => PaperSubmission::where('conferences_id',$conference_id)->with('author')->get(),
    ]);
  }

  public function submissionDetails($id)
  {

    return view('editorial-manager.paper_submissions.submitted_paper_details')->with([
      'paper' => PaperSubmission::where('id',$id)->with('author')->first(),
      'authors' => Author::where('paper_submission_id',$id)->get(),
    ]);
  }
}
