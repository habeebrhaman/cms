<?php

namespace App\Http\Controllers;

use App\Chair;
use Illuminate\Http\Request;
use App\Conference;
use Auth;

class ChairController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:editorlogin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        return view('editorial-manager.chair.chair')->with([
            'rows' => Chair::where('conferences_id',$conference_id)->get(),

           ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        $chair = new Chair;
        $chair->name = $request->name;
        $chair->program_sessions_id = $request->session_id;
        $chair->conferences_id = $conference_id;
        $chair->save();
        return redirect()->back()->with([
            'rows' => Chair::where('conferences_id',$conference_id)->get(),
            ]);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chair  $chair
     * @return \Illuminate\Http\Response
     */
    public function show(Chair $chair)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chair  $chair
     * @return \Illuminate\Http\Response
     */
    public function edit(Chair $chair)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chair  $chair
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chair $chair)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chair  $chair
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chair = Chair::findOrFail($id);
        $chair->delete();
    }
}
