<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use App\ConferenceReviewer;
use App\PaperSubmission;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $review = new Review();

        $review->paper_id = $request->paper_id;
        $review->commands = $request->commands;
        $review->status = $request->status_id;
        $review->conference_rev_id = $request->conf_rev_id;

        $review->save();
        $id = ConferenceReviewer::where([['paper_id',$request->paper_id],['status',1]])->pluck('id')->first();
        $confrev = ConferenceReviewer::find($id);
        $confrev->status = 2;
        $confrev->save();

        $paper_update = PaperSubmission::find($request->paper_id);
        $paper_update->review_status = $request->status_id;
        $paper_update->save();



        return view('reviewer.completed_papers')->with([
          'rows' => ConferenceReviewer::orderBy('id','DESC')->where([['status',2],['is_inactive',0]])->get(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $review = Review::find($id);
      $review->paper_id = $request->paper_id;
      $review->commands = $request->commands;
      $review->status = $request->status_id;
      $review->save();

      $paper_update = PaperSubmission::find($request->paper_id);
      $paper_update->review_status = $request->status_id;
      $paper_update->save();

      return view('reviewer.completed_papers')->with([
        'rows' => ConferenceReviewer::orderBy('id','DESC')->where([['status',2],['is_inactive',0]])->get(),
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }
}
