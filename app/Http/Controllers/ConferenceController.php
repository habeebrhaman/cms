<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Services\Conference as ConferenceResource;
use App\Http\Resources\Services\ConferenceCollection;
use App\Http\Requests\ConferenceRequest;
use App\Conference;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('conference.create-conference');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConferenceRequest $request)
    {
        $conference = new Conference;
          $conference->name = $request-> name;
          $conference->acronym = $request-> acronym;
          $conference->web = $request-> web;
          $conference->email = $request-> email;
          $conference->venue = $request-> venue;
          $conference->city = $request-> city;
          $conference->country = $request-> country;
          $conference->startdate = $request-> startdate;
          $conference->enddate = $request-> enddate;
          $conference->year = $request-> year;
          $conference->about = $request-> about;
          $conference->is_published = "0";

          $conference->save();
          return view('conference.conference_confirm');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conference $conference)
    {
      $conference->name = $request-> name;
      $conference->acronym = $request-> acronym;
      $conference->web = $request-> web;
      $conference->email = $request-> email;
      $conference->venue = $request-> venue;
      $conference->city = $request-> city;
      $conference->country = $request-> country;
      $conference->startdate = $request-> startdate;
      $conference->enddate = $request-> enddate;
      $conference->year = $request-> year;
      $conference->about = $request-> about;
      $conference->is_published = "1";

      $conference->save();

      return redirect('/editorlogin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
