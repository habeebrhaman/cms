<?php

namespace App\Http\Controllers;

use App\ProgramSession;
use Illuminate\Http\Request;

class ProgramSessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:editorlogin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProgramSession  $programSession
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramSession $programSession)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProgramSession  $programSession
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramSession $programSession)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProgramSession  $programSession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramSession $programSession)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProgramSession  $programSession
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramSession $programSession)
    {
        //
    }
}
