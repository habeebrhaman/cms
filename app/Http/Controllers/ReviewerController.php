<?php

namespace App\Http\Controllers;

use App\Reviewer;
use Illuminate\Http\Request;
use App\TopicArea;
use App\ConferenceReviewer;
use App\PaperSubmission;
use App\Author;
use App\Review;

class ReviewerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

     public function showPage()
     {
       $user_id = auth()->user()->id;
         return view('reviewer.review_papers')->with([
           'rows' => ConferenceReviewer::orderBy('id','DESC')->where([['status',0],['reviewer_id',$user_id],['is_inactive',0]])->get(),
         ]);
     }

     public function showCurrentPage()
     {
       $user_id = auth()->user()->id;
         return view('reviewer.review_papers')->with([
           'rows' => ConferenceReviewer::orderBy('id','DESC')->where([['status',1],['reviewer_id',$user_id],['is_inactive',0]])->get(),
         ]);
     }

     public function showDetails($id)
     {
        $user_id = auth()->user()->id;
       $paper_id = PaperSubmission::where('id',$id)->pluck('id')->first();

       $confrev_id = ConferenceReviewer::where([['paper_id',$paper_id],['status',0],['reviewer_id',$user_id]])->pluck('id')->first();
       if($confrev_id>0)
       {
         $confrev = ConferenceReviewer::find($confrev_id);
         $confrev->status = 1;
         $confrev->save();
       }
       return view('reviewer.paper_view')->with([
         'paper' => PaperSubmission::where('id',$id)->with('author')->first(),
         'authors' => Author::where('paper_submission_id',$id)->get(),
       ]);
     }

     public function showSubmitPage($id)
     {
       return view('reviewer.submit_review')->with([
         'paper' => PaperSubmission::where('id',$id)->with('author')->first(),
         'conf_rev_id' => ConferenceReviewer::orderBy('id','DESC')->where('paper_id',$id)->first(),

       ]);
     }

     public function showCompleted()
     {
       $user_id = auth()->user()->id;
         return view('reviewer.completed_papers')->with([
           'rows' => ConferenceReviewer::orderBy('id','DESC')->where([['status',2],['reviewer_id',$user_id],['is_inactive',0]])->get(),
         ]);
     }

     public function showReviewDetailsPage($id)
     {
            $paper_id = Review::where('id',$id)->pluck('paper_id')->first();
            return view('reviewer.review_details')->with([
           'row' => Review::orderBy('id','DESC')->where('id',$id)->first(),
           'conf_rev_id' => ConferenceReviewer::orderBy('id','DESC')->where('paper_id',$paper_id)->first(),
         ]);
     }
}
