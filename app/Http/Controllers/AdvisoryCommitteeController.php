<?php

namespace App\Http\Controllers;

use App\AdvisoryCommittee;
use Illuminate\Http\Request;
use Auth;
use App\Conference;

class AdvisoryCommitteeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:editorlogin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        return view('editorial-manager.advisory.advisory')->with([
            'rows' => AdvisoryCommittee::where('conferences_id',$conference_id)->get(),

           ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = Auth::user()->email;
        $conference_id = Conference::where('email' ,$email)->pluck('id')->first();
        $adv = new AdvisoryCommittee;
        $adv->name = $request->name;
        $adv->conferences_id = $conference_id;
        $adv->save();
        return redirect()->back()->with([
            'rows' => AdvisoryCommittee::where('conferences_id',$conference_id)->get(),
            ]);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdvisoryCommittee  $advisoryCommittee
     * @return \Illuminate\Http\Response
     */
    public function show(AdvisoryCommittee $advisoryCommittee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdvisoryCommittee  $advisoryCommittee
     * @return \Illuminate\Http\Response
     */
    public function edit(AdvisoryCommittee $advisoryCommittee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdvisoryCommittee  $advisoryCommittee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdvisoryCommittee $advisoryCommittee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdvisoryCommittee  $advisoryCommittee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adv = AdvisoryCommittee::findOrFail($id);
        $adv->delete();
    }
}
