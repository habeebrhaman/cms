<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TopicArea;
use App\ReviewerTopic;

class RreviewerTopic extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
      return view('reviewer.add_topics')->with([
        'Topics' => TopicArea::where('parent_id',0)->get(),
        'rows' => ReviewerTopic::orderBy('id','DESC')->with('topics')->paginate(10),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user_id = auth()->user()->id;
        $add = new ReviewerTopic();

        $add->user_id = $user_id;
        $add->topic_id = $request->topic_id;

        $add->save();
        return view('reviewer.add_topics')->with([
          'Topics' => TopicArea::where('parent_id',0)->get(),
          'rows' => ReviewerTopic::orderBy('id','DESC')->with('topics')->paginate(10),
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = ReviewerTopic::find($id);
        $topic->delete();
        return view('reviewer.add_topics')->with([
          'Topics' => TopicArea::where('parent_id',0)->get(),
          'rows' => ReviewerTopic::orderBy('id','DESC')->with('topics')->paginate(10),
        ]);
    }
}
