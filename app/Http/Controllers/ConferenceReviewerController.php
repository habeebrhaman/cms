<?php

namespace App\Http\Controllers;

use App\ConferenceReviewer;
use Illuminate\Http\Request;
use App\User;
use App\ReviewerTopic;
use App\Editorlogin;
use App\PaperSubmission;
use App\Review;
use App\Author;
use Mail;
use App\PaperMail;
use App\Notification;
use App\FinalPaper;
use App\Conference;


class ConferenceReviewerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth:editorlogin');
     }
    public function index()
    {
        return view('editorial-manager.reviewer.reviewer_list')->with([
          'rows' => User::where('is_reviewer',1)->get(),
        ]);
    }
    public function paperAssign()
    {
      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
        return view('editorial-manager.reviewer.assign_paper')->with([
          'rows' => ConferenceReviewer::orderBy('id','DESC')->get(),
          'papers' => PaperSubmission::where('conferences_id',$conf_id)->get(),
          'reviewer' => User::where('is_reviewer',1)->get(),
        ]);
    }
    public function assignToReviewer(Request $request)
    {
      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
      $is_rev = ConferenceReviewer::where('paper_id',$request->paper_id)->get();
      $authors = Author::where('paper_submission_id',$request->paper_id)->get();
      $rev_number = $is_rev->count();
      $authors_number = $authors->count();

      $old_id = ConferenceReviewer::where([['paper_id',$request->paper_id],['is_inactive',0]])->orderBy('id','DESC')->first();

      $new = new ConferenceReviewer();
      $new->conference_id = $conf_id;
      $new->reviewer_id = $request->reviewer_id;
      $new->paper_id = $request->paper_id;
      if($rev_number <= 0)
      {
        $new->paper_generated_id = $conf_id."-"."FR-"."-".$authors_number;
      }
      else {
          $new->paper_generated_id = $conf_id."-"."R".$rev_number."-".$authors_number;
          $new->rev_status = $rev_number;

          $update_rev = ConferenceReviewer::find($old_id->id);
          $update_rev->is_inactive = 1;
          $update_rev->save();
      }
      $new->save();

      return view('editorial-manager.reviewer.assign_paper')->with([
        'rows' => ConferenceReviewer::orderBy('id','DESC')->where('is_inactive',0)->get(),
        'papers' => PaperSubmission::where('conferences_id',$conf_id)->get(),
        'reviewer' => User::where('is_reviewer',1)->get(),
      ]);


    }

    public function manageReviewer()
    {
      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
      return view('editorial-manager.reviewer.manage_reviewer')->with([
        'rows' => ConferenceReviewer::where('conference_id',$conf_id)->get(),
      ]);
    }

    public function reviewList()
    {
      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
      return view('editorial-manager.reviewer.reviews')->with([
        'rows' => ConferenceReviewer::orderBy('id','DESC')->where([['status',2],['is_inactive',0]])->get(),
      ]);
    }

    public function reviewsDetails($id)
    {
      return view('editorial-manager.reviewer.review_details')->with([
        'row' => Review::orderBy('id','DESC')->where('id',$id)->first(),
      ]);
    }

    public function reviewsUpdate(Request $request, $id)
    {
      $review = Review::find($id);
      $review->paper_id = $request->paper_id;
      $review->commands = $request->commands;
      $review->status = $request->status_id;
      $review->save();

      $paper_update = PaperSubmission::find($request->paper_id);
      $paper_update->review_status = $request->status_id;
      $paper_update->save();

      return view('editorial-manager.reviewer.reviews')->with([
        'rows' => ConferenceReviewer::orderBy('id','DESC')->where([['status',2],['is_inactive',0]])->get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConferenceReviewer  $conferenceReviewer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('editorial-manager.reviewer.reviewers_details')->with([
        'row' => User::where('id',$id)->first(),
        'topic' => ReviewerTopic::where('user_id',$id)->with('topics')->get(),
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConferenceReviewer  $conferenceReviewer
     * @return \Illuminate\Http\Response
     */
    public function edit(ConferenceReviewer $conferenceReviewer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConferenceReviewer  $conferenceReviewer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConferenceReviewer $conferenceReviewer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConferenceReviewer  $conferenceReviewer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
        $confrev = ConferenceReviewer::find($id);
        $confrev->delete();
        return view('editorial-manager.reviewer.assign_paper')->with([
          'rows' => ConferenceReviewer::orderBy('id','DESC')->get(),
          'papers' => PaperSubmission::where('conferences_id',$conf_id)->get(),
          'reviewer' => User::where('is_reviewer',1)->get(),
        ]);
    }

    public function showRivision($id)
    {

      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
        return view('editorial-manager.reviewer.assign_paper')->with([
          'rows' => ConferenceReviewer::orderBy('id','DESC')->get(),
          'papers' => PaperSubmission::where('conferences_id',$conf_id)->get(),
          'reviewer' => User::where('is_reviewer',1)->get(),
          'old' =>ConferenceReviewer::orderBy('id','DESC')->where('paper_id',$id)->first(),
        ]);
    }

    public function acceptedPapers()
    {
      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
      return view('editorial-manager.paper_submissions.accepted_papers')->with([
        'papers' => ConferenceReviewer::orderBy('id','DESC')->where([['status',2],['is_inactive',0]])->with('paper')->with('mailSender')->whereHas('paper', function ($query) {
            $query->where('review_status',0)->orWhere('review_status',1);
        })->get(),
      ]);
    }

    public function finalPapers()
    {
      $user_id = auth()->user()->id;
      $conf_id = Editorlogin::where('id',$user_id)->pluck('conferences_id')->first();
      return view('editorial-manager.paper_submissions.final_papers')->with([
        'papers' => FinalPaper::where('conference_id',$conf_id)->get(),
      ]);
    }
    public function sendAcceptMail($id)
    {
      $paper_id = ConferenceReviewer::where('id',$id)->pluck('paper_id')->first();
      $papers = PaperSubmission::where('id',$paper_id)->first();

      $user_id = User::where('id',$papers->user_id)->first();

      Mail::send('accept-email',
        array(
            'email' => $user_id->email,
            'user_id' => $user_id->id,
        ),
       function($message) use($user_id)
       {
           $message->from('habeeb.pth@outlook.com');
           $message->to($user_id->email, 'Admin')->subject('Editorial Manager');

       });
       $update = new PaperMail();
       $update->conference_rev_id = $id;
       $update->accept_mail_send = 1;
       $update->final_paper_mail_send = 0;

       $update->save();
       return redirect()->back()->with('message','mail send successfully');
    }

    public function showNotifications()
    {
      return view('editorial-manager.notifications')->with([
        'rows' => Notification::orderBy('id','DESC')->get(),
      ]);
    }

    public function showConferenceDetails()
    {
      $user_id = auth()->user()->id;
      return view('conference.conference_details')->with([
        'conf' => Conference::with('editor')->whereHas('editor', function ($query)use ($user_id) {
            $query->where('id',$user_id);
        })->first(),
      ]);
    }

}
