<?php

namespace App\Http\Resources\Services;

use Illuminate\Http\Resources\Json\JsonResource;
class Job extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => 'required|max:255',
              'acronym' => 'required',
              'web' => 'required|max:255',
              'email' => 'required|email|unique:conference',
              'venue' => 'required|max:255',
              'city' => 'required|max:255',
              'startdate' => 'required',
              'enddate' => 'required',
              'year' => 'required|max:255',
            
        ];
    }
}
