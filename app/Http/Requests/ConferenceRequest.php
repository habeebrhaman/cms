<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Conference;
class ConferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
              'acronym' => 'required',
              'web' => 'required|max:255',
              'email' => 'required|email|unique:conferences',
              'venue' => 'required|max:255',
              'city' => 'required|max:255',
              'startdate' => 'required',
              'enddate' => 'required',
              'year' => 'required|max:255',
        ];
        
    }
}
