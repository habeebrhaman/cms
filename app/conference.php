<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{
  protected $fillable = [
      'name','acronym','web','email','venue','city','country','startdate','enddate','year',
  ];

  public $table ='conferences';

  protected $hidden = [
    'password', 'remember_token',
];

      public function chair()
      {
             return $this->hasMany('App\Chair','conferences_id');
      }
      public function callForPaper()
      {
             return $this->hasOne('App\CallForPaper','conferences_id');
      }
      public function technical()
      {
            return $this->hasMany('App\TechnicalCommittee','conferences_id');
      }

      public function advisory()
      {
             return $this->hasMany('App\AdvisoryCommittee','conferences_id');
      }

      public function editor()
      {
             return $this->hasOne('App\Editorlogin','conferences_id');
      }
}
