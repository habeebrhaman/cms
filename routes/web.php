<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomePage@index')->name('home-page');


Route::get('/conferences/{id}','ConferencePage@showConference');



Route::get('/conference_confirm', function () {
  return view('conference.conference_confirm');
});

Auth::routes();
Route::resource('add_topics','RreviewerTopic');
Route::get('/create-conference', 'ConferenceController@index')->name('create-conference');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/review', 'HomeController@reviewIndex')->name('review');
Route::resource('new_submission', 'PaperSubmissionController');
Route::resource('final_paper_upload','FinalPaperController');
Route::get('/final_upload_page','HomeController@showFinalPage');
Route::get('home_submissions','HomeController@showSubmissions');
Route::get('update/{id}','HomeController@updatePage');

Route::group(['middleware' => ['web','iefix'],'prefix'=>'admin'], function () {
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.admin');
});
Route::group(['middleware' => ['web','iefix'],'prefix'=>'editorlogin'], function () {
  Route::get('/editorlogin', 'Auth\editorLoginController@showLoginForm')->name('editor.login');
  Route::post('/editorlogin', 'Auth\editorLoginController@login')->name('editor.login.submit');
  Route::get('/', 'EditorController@index')->name('conference.conferences');
});

Route::resource('conference', 'ConferenceController');
Route::resource('chair', 'ChairController');
Route::resource('advisory', 'AdvisoryCommitteeController');
Route::resource('technical', 'TechnicalCommitteeController');
Route::resource('call_for_paper', 'CallForPaperController');
Route::get('submissions','SubmittedPaperController@index');
Route::get('submission_details/{id}','SubmittedPaperController@submissionDetails');
Route::get('test','SubmittedPaperController@test');
Route::resource('reviewers_list','ConferenceReviewerController');
Route::get('paper_assign','ConferenceReviewerController@paperAssign');
Route::post('assign_to_reviewer','ConferenceReviewerController@assignToReviewer');
Route::get('manage_reviewer','ConferenceReviewerController@manageReviewer');
Route::get('reviews','ConferenceReviewerController@reviewList');
Route::get('reviews_details/{id}','ConferenceReviewerController@reviewsDetails');
Route::patch('paper_review_update/{id}','ConferenceReviewerController@reviewsUpdate');
Route::get('assign_revision/{id}','ConferenceReviewerController@showRivision');
Route::get('accepted_papers','ConferenceReviewerController@acceptedPapers');
Route::get('send_accept_mail/{id}','ConferenceReviewerController@sendAcceptMail');
Route::get('show_notifications','ConferenceReviewerController@showNotifications');
Route::resource('notifications','NotificationController');
Route::get('final_papers','ConferenceReviewerController@finalPapers');
Route::get('conference_details','ConferenceReviewerController@showConferenceDetails');


// admins controll

Route::resource('/create_topics','TopicAreaController');
Route::get('new_conferences','AdminController@getNew');
Route::get('accept_conference/{id}','AdminController@acceptConf');
Route::get('reviewer_registration','Auth\RegisterController@showRegForm');

Route::post('/login/custom',[
  'uses' => 'Auth\LoginController@login',
  'as' => 'login.custom'
]);

// reviewer contrls

Route::get('review_papers','ReviewerController@showPage');
Route::get('review_paper/{id}','ReviewerController@showReviewDetailsPage');
Route::get('review_details/{id}','ReviewerController@showReviewDetailsPage');
Route::get('review_current_papers','ReviewerController@showCurrentPage');
Route::get('completed_papers','ReviewerController@showCompleted');
Route::get('review_paper_details/{id}','ReviewerController@showDetails');
Route::get('submit_review_page/{id}','ReviewerController@showSubmitPage');
Route::resource('paper_review','ReviewController');
